<?php

return [
    '<alias:index>' => 'site/<alias>',
    '<alias:/select-sim/<customerId:\d+>>' => 'sim/<alias>',
    '<alias:/remove-sim/<customerId:\d+><imsiId:\d+>>' => 'sim/<alias>',
    '<alias:/list>' => 'sim/<alias>',
];

<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/learning',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Flearning%2F',
                'enableAutoLogin' => false,
            ],
            'urlManager' => [
                'enableStrictParsing' => false,
            ],
        ],
    ];
};

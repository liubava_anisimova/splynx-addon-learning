<?php

namespace app\commands;

use splynx\base\BaseInstallController;

/**
 * Class InstallController
 * @package app\commands
 */
class InstallController extends BaseInstallController
{
    /**
     * @inheritdoc
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @inheritdoc
     */
    public static $minimumSplynxVersion = '3.1';

    /**
     * @inheritdoc
     * @return string
     */
    public function getAddOnTitle()
    {
        // Db column varchar(64)
        return 'Splynx Add-on Learning';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        // Db column varchar(32)
        return 'splynx_addon_learning';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Locations',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Partners',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view', 'update'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'learning_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Flearning%2F',
                'icon' => 'fa-puzzle-piece',
            ],
            [
                'name' => 'learning_sim_list',
                'title' => 'Sim Card',
                'root' => 'controllers\admin\NetworkingController',
                'place' => 'admin',
                'url' => '%2Flearning%2Fsim%2Flist',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'customers',
                'module' => $this->getModuleName(),
                'name' => 'sim_card',
                'required' => 'false',
                'type' => 'string',
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function getHooks()
    {
        $hook = [
            'title' => $this->getAddOnTitle(),
            'type' => 'cli',
            'path' => urlencode(static::getSplynxDir() . 'addons/splynx-addon-learning/yii hook'),
            'events' => [
                [
                    'model' => 'models\common\customers\Customers',
                    'actions' => ['edit', 'delete'],
                ],
            ],
        ];
        if ($this->isInstallProcess()) {
            $hook['enabled'] = 'true';
        }
        return [$hook];
    }
}

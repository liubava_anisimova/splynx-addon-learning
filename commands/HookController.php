<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Customer;
use yii\helpers\Console;
use yii\helpers\Json;
use Yii;

/**
 * Class SiteController
 * @package app\controllers
 */
class HookController extends Controller
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $data = Json::decode(Console::stdin(true));
        echo Yii::t('app', 'Found model "{model}"', ['model' => $data['model'],]) . "\n";

        if ($data['result'] != 'success') {
            echo Yii::t('app', 'Hook result is different from "success"') . "\n";
            return;
        }

        if (empty($data['changed_attributes']['status'])) {
            echo Yii::t('app', 'Invalid action. Works only with changing status') . "\n";
            return;
        }

        (new Customer())->runHook($data);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%imsi}}`.
 */
class m210529_154502_create_imsi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%imsi}}', [
            'id' => $this->primaryKey(),
            'imsi' => $this->integer(),
            'partner_id' => $this->integer(),
            'location_id' => $this->integer(),
            'used_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%imsi}}');
    }
}

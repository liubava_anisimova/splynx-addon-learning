<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;
use yii\helpers\ArrayHelper;
use app\components\ConsoleLog;
use Yii;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{
    /**
     * @return Customer[]
     */
    public function getCustomers()
    {
        $customers = $this->findAll(
            [],
            [],
            [],
            12,
            3
        );

        if ($customers == null) {
            Yii::$app->getSession()->setFlash('error', 'Customer has not been found.');
        }

        ArrayHelper::multisort($customers, 'login');
        return $customers;
    }

    /**
     * @return string
     * @throws \splynx\base\ApiResponseException
     */
    public function getPartnerName()
    {
        return (new Partner)->getPartnerNameById($this->partner_id);
    }

    /**
     * @return mixed
     * @throws \splynx\base\ApiResponseException
     */
    public function getLocationName()
    {
        return (new Location)->getLocationNameById($this->location_id);
    }

    public function runHook($data)
    {
        ConsoleLog::log(Yii::t('app', 'Found changing status to \'{status}\'', ['status' => $data['attributes']['status']]));

        if ($data['attributes_additional']['sim_card'] === '') {
            return;
        }

        if ($data['attributes']['status'] === 'blocked' || $data['attributes']['status'] === 'disabled') {
            return $this->removeSimCardFromCustomer($data['attributes']['id']);
        }
    }

    /**
     * @param int $customerId
     * @return bool
     */
    public function removeSimCardFromCustomer(int $customerId)
    {
        $customer = Customer::findIdentity($customerId);
        if ($customer == null) {
            return false;
        }
        if ($customer->additional_attributes['sim_card'] != '') {
            $customer->additional_attributes['sim_card'] = '';
            $unusedSimCards = (new Imsi)->makeUnusedSimCards($customerId);

            if ($customer->save() && $unusedSimCards) {
                return true;
            }
            return false;
        }
    }

    /**
     * @param int $customerId
     * @return bool
     */
    public function checkIfBlockedOrInactive(int $customerId)
    {
        $customer = Customer::findIdentity($customerId);
        if ($customer->status == 'blocked' || $customer->status == 'disabled') {
            Yii::$app->getSession()->setFlash('error', 'Can\'t select Sim Card. ' . $customer->name . ' has ' . $customer->status . '.');
            return false;
        }
        return true;
    }
}

<?php

namespace app\models;

use splynx\v2\models\administration\BasePartner;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Partner
 * @package app\models
 */
class Partner extends BasePartner
{
    public static $partnerNames = [];

    /**
     * @param int $id
     * @return string
     * @throws \splynx\base\ApiResponseException
     */
    public function getPartnerNameById(int $id)
    {
        if (!isset(static::$partnerNames[$id])) {
            $partner = $this->findById($id);
            static::$partnerNames[$id] = $partner ? $partner->name : 'Deleted';
        }

        return static::$partnerNames[$id];
    }

    /**
     * @return array|bool
     */
    public static function getAllPartnerNames()
    {
        $allPartners = (new Partner)->findAll([]);

        if ($allPartners == null) {
            Yii::$app->getSession()->setFlash('error', 'Partner has not been found.');
            return false;
        }

        return (new Partner)->getArrayNamePartners($allPartners);
    }

    /**
     * @param array $allPartners
     * @return array
     */
    public function getArrayNamePartners(array $allPartners)
    {
        return ArrayHelper::map($allPartners, 'id', 'name');
    }
}

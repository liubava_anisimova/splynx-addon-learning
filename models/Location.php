<?php

namespace app\models;

use splynx\v2\models\administration\BaseLocation;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Location
 * @package app\models
 */
class Location extends BaseLocation
{
    public static $locationNames = [];

    /**
     * @param int $id
     * @return mixed
     * @throws \splynx\base\ApiResponseException
     */
    public function getLocationNameById(int $id)
    {
        if (!isset(static::$locationNames[$id])) {
            $location = $this->findById($id);
            static::$locationNames[$id] = $location ? $location->name : 'Deleted';
        }

        return static::$locationNames[$id];
    }

    /**
     * @return array|bool
     */
    public static function getAllLocationNames()
    {
        $allLocations = (new Location)->findAll([]);

        if ($allLocations == null) {
            Yii::$app->getSession()->setFlash('error', 'Location has not been found.');
            return false;
        }

        return (new Location)->getNameLocations($allLocations);
    }

    /**
     * @param array $allLocations
     * @return array
     */
    public function getNameLocations(array $allLocations)
    {
        return ArrayHelper::map($allLocations, 'id', 'name');
    }
}

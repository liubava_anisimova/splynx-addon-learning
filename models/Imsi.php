<?php

namespace app\models;

use splynx\base\ApiResponseException;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Customer
 * @package app\models
 */
class Imsi extends ActiveRecord
{
    public static $customerNames = [];
    public static $simCardNames = [];

    public function rules()
    {
        return [
            [['imsi', 'partner_id', 'location_id'], 'required'],
            [
                'imsi',
                'unique',
                'targetAttribute' => ['imsi'],
                'message' => 'IMSI must be unique.'
            ],
            [
                'imsi',
                'match',
                'pattern' => '/^[0-9]{12}$/',
                'message' => Yii::t('app', 'IMSI must contain exactly 12 digits'),
            ],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%imsi}}';
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getAllSimCards()
    {
        return Imsi::find()->all();
    }

    /**
     * @param int $used_by
     * @return int|mixed
     */
    public function getCustomerNameById(int $used_by)
    {
        if ($used_by == 0) {
            return $used_by;
        }
        if (!isset(static::$customerNames[$used_by])) {
            $customer = (new Customer)->findIdentity($used_by);
            static::$customerNames[$used_by] = $customer ? $customer->name : 'Deleted';
        }
        return static::$customerNames[$used_by];
    }

    /**
     * @param int $id
     * @return string
     * @throws ApiResponseException
     */
    public function getPartnerNameById(int $id)
    {
        return (new Partner)->getPartnerNameById($id);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ApiResponseException
     */
    public function getLocationNameById(int $id)
    {
        return (new Location)->getLocationNameById($id);
    }

    /**
     * @param int $customerId
     * @return array|bool
     * @throws ApiResponseException
     */
    public static function getFreeSimCardsListForCustomer(int $customerId)
    {
        $customer = (new Customer())->findById($customerId);
        $freeSimCards = static::find()->where([
            'partner_id' => $customer->partner_id,
            'location_id' => $customer->location_id,
            'used_by' => 0
        ])->all();

        if ($customer == null || $freeSimCards == null) {
            return false;
        }

        return ArrayHelper::map($freeSimCards, 'imsi', 'imsi');
    }

    /**
     * @param int $customerId
     * @return array|bool|ActiveRecord|null
     */
    public function makeUnusedSimCards(int $customerId)
    {
        $busiedImsi = static::find()->where(['used_by' => $customerId])->one();

        if (!empty($busiedImsi)) {
            $busiedImsi->used_by = 0;

            if ($busiedImsi->save(false)) {
                return $busiedImsi;
            }
        }
        return false;
    }

    /**
     * @param string $params
     * @param int $customerId
     * @return bool
     * @throws ApiResponseException
     */
    public function saveSelectedSimCard(string $params, int $customerId)
    {
        $customer = (new Customer)->findById($customerId);
        $imsi = static::find()->where(['imsi' => $params])->one();

        if (empty($customer) || empty($imsi)) {
            return false;
        }

        $customer->additional_attributes['sim_card'] = $params;
        $imsi->used_by = $customerId;

        if ($customer->save() && $imsi->save()) {
            return true;
        }
    }
}

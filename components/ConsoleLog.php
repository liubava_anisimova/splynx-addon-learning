<?php

namespace app\components;

use yii\base\Component;

/**
 * Class ConsoleLog
 * @package app\components
 */
class ConsoleLog extends Component
{
    /**
     * Echo message with date and time
     * @param $message
     */
    public static function log($message)
    {
        echo date("Y-m-d H:i:s") . " " . $message . "\n";
    }
}

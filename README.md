Splynx Add-on Learning
======================

Splynx Add-on Learning based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base-2.git
cd splynx-addon-base-2
composer install
~~~

Install Splynx Add-on Learning:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:liubava_anisimova/splynx-addon-learning.git
cd splynx-addon-learning
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-learning/web/ /var/www/splynx/web/learning
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-learning.addons
~~~

with following content:
~~~
location /learning
{
        try_files $uri $uri/ /learning/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Learning in a menu "Customers".
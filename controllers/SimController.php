<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Imsi;
use app\models\Partner;
use app\models\Location;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\Response;

/**
 * Class SiteController
 * @package app\controllers
 */
class SimController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $this->getView()->title = 'List of Sim Cards';

        return $this->render('list', [
            'model' => new Imsi()
        ]);
    }

    /**
     * @param int $customerId
     * @return array|string
     * @throws \splynx\base\ApiResponseException
     */
    public function actionSelectSim(int $customerId)
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $params = Yii::$app->request->post();

            if ((new Customer)->checkIfBlockedOrInactive($customerId) == false) {
                return $this->redirect('/learning');
            }

            $imsi = new Imsi();
            if ($params) {
                $imsi->makeUnusedSimCards($customerId);
                if ($imsi->saveSelectedSimCard($params['imsi'], $customerId)) {
                    return [
                        'result' => true,
                        'message' => 'Sim card has been selected!',
                    ];
                }
            }
            return [
                'result' => false,
                'message' => 'Adding Sim Card has been failed',
            ];
        }
        return $this->render('select-sim', [
            'simCardsList' => Imsi::getFreeSimCardsListForCustomer($customerId)
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionAddSim()
    {
        $allNamePartners = Partner::getAllPartnerNames();
        $allNameLocations = Location::getAllLocationNames();
        $form = new Imsi();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->used_by = 0;

            if ($form->save()) {
                Yii::$app->getSession()->setFlash('success', 'Sim Card has been successfully added.');
                $this->getView()->title = 'Add Sim Card';
                return $this->redirect('/learning/sim/list');
            }

            Yii::$app->getSession()->setFlash('error', 'Sim Card has not been added.');
        }
        return $this->render('add-sim', ['model' => $form, 'allNamePartners' => $allNamePartners,
            'allNameLocations' => $allNameLocations]);
    }

    /**
     * @param int $customerId
     * @param int $imsiId
     * @return Response
     * @throws \splynx\base\ApiResponseException
     */
    public function actionRemoveSim(int $customerId, int $imsiId)
    {
        $simCard = Imsi::findOne($imsiId);
        $form = (new Customer)->findById($customerId);

        if ($simCard == null || $form == null) {
            Yii::$app->getSession()->setFlash('error', 'Remove Sim Card has been failed.');
        }

        $simCard->used_by = 0;
        $form->additional_attributes['sim_card'] = '';

        if ($simCard->save(false) && $form->save(false)) {
            Yii::$app->getSession()->setFlash('success', 'Sim Card has been successfully deleted from Customer.');
            return $this->redirect('/learning/sim/list');
        }
        Yii::$app->getSession()->setFlash('error', 'Sim Card has not been deleted from Customer.');
    }
}
